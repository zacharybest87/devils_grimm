/**
 * This class returns sprite objects based on strings
 */

function SpriteFactory(game){
	this.game = game;
};
SpriteFactory.prototype.createSprite = function(name){
	var sprite;
	switch (name){
		case 'player':		
			sprite = new Player(this.game); 		break;
		default: sprite = null;
	}
	return sprite;
};