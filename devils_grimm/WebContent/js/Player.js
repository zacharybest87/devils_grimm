/**
 * This class represents the player controlled character.
 * Animations and Movement exist here.
 * @param game the game in which to load the sprite
 * @returns a Player sprite object
 */

Player = function(game){
	this.game = game;
	this.animationNames = ['idleLeft', 'idleRight', 'walkLeft', 'walkRight'];
	this.controller = {
		up: 	game.input.keyboard.addKey(Phaser.Keyboard.W),
		down:	game.input.keyboard.addKey(Phaser.Keyboard.S),
		left:	game.input.keyboard.addKey(Phaser.Keyboard.A),
		right:	game.input.keyboard.addKey(Phaser.Keyboard.D),
		jump:	game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)
	};
	this.state = 'idle';
	this.movementSpeed = 150;
	this.jumpSpeed = 256;
	this.jumpSpeed *= -1;
	this.direction = 0;
	this.airJumpsDefault = 1;
	this.airJumps = 1;
	this.audio = {};
};
Player.prototype = Object.create(Phaser.Sprite.prototype);
Player.prototype.constructor = Player;
Player.prototype.getDirection = function(){
	if (this.direction < 0){
		return 'Left';
	}
	return 'Right';
};

Player.prototype.preload = function(){
	this.game.load.atlasJSONHash('player', 'assets/rogue.png', 'assets/rogue.json');
	this.game.load.audio('jump', 'assets/rogueJump.wav');
};
Player.prototype.create = function(){
	this.createGraphics();
	this.createAudio();
	this.createPhysics();
	this.game.add.existing(this);
};
Player.prototype.update = function(){
	this.updateMovement();
};

Player.prototype.setSpawn = function(spawnPoint){
	this.spawnPoint = spawnPoint;
};

Player.prototype.createGraphics = function(){
	this.smoothed = false;
	for (var i = 0; i < this.animationNames.length; i++){
		var animationName = this.animationNames[i];
		this.animations.add(animationName, Phaser.Animation.generateFrameNames(animationName+'_', 0, 9, '.png', 4), 10, true);
	}
	this.animations.add('jumpLeft', Phaser.Animation.generateFrameNames('jumpLeft' + '_', 0, 2, '.png', 4), 2, false);
	this.animations.add('jumpRight', Phaser.Animation.generateFrameNames('jumpRight' + '_', 0, 2, '.png', 4), 2, false);
};

Player.prototype.createAudio = function(){
	this.audio.jump = this.game.add.audio('jump');
}

Player.prototype.createPhysics = function(){
	this.game.physics.arcade.enable(this); 
	this.body.gravity.y = 300;
	this.body.collideWorldBounds = true;
};

Player.prototype.updateMovement = function(){
	var animation;
	
	//Reset Horizontal Movement
    this.body.velocity.x = 0;
    
    //Calculate Horizontal Velocity
    var hVelocity = 0;
    if (this.controller.left.isDown) {
        this.direction = -1; //left
        hVelocity = this.movementSpeed * this.direction;
    }
    if (this.controller.right.isDown) {
        this.direction = 1; //right
        hVelocity = this.movementSpeed * this.direction;
    }
    animation = hVelocity != 0 ? 'walk' + this.getDirection() : 'idle' + this.getDirection();
    
    //Calculate Vertical Velocity
    var vVelocity = this.body.velocity.y;
    if (this.controller.jump.justDown 
    && (this.body.blocked.down || this.airJumps-- > 0)){
    	vVelocity = this.jumpSpeed; //Jump ascend
    	this.audio.jump.play();
    }
    if (this.controller.jump.isUp && this.body.velocity.y < 0){
    	vVelocity *= 0.98; //Jump descend
    }
    if (vVelocity != 0){
    	animation = 'jump' + this.getDirection();
    	this.state = 'jumped';
    } else if (this.body.blocked.down){
    	this.state = 'grounded';
    	this.airJumps = this.airJumpsDefault;
    }
    if (this.body.velocity.y != 0){
    	this.state = 'airborne';
    }
    
    
    //Perform Animation
    if (this.state != 'airborne'){
    	this.animations.play(animation);
    }
    
    //Move
    this.body.velocity.x = hVelocity;
    this.body.velocity.y = vVelocity;
}
