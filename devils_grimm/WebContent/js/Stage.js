/**
 * This class represents a map/level/stage
 */

function Stage(game, spriteFactory){
	this.game = game;
	this.spriteFactory = spriteFactory;
	this.map;
	this.gameObjects = [];
	this.backgroundMusic;
}
Stage.prototype.preload = function(){
	this.game.load.onFileComplete.add(this.preloadSprites, this);
	this.game.load.tilemap('tilemap', 'assets/demoMap.json', null, Phaser.Tilemap.TILED_JSON);
	this.game.load.image('tiles', 'assets/demotiles.png');	
	this.game.load.audio('Neoakushon', 'assets/Rolemusic_Neoakushon.mp3');
};

/**
 * Checks to see if the map is loaded and if so,
 * It will detect the sprites contained in the object layer
 * and preload their required resources.
 */
Stage.prototype.preloadSprites = function(){
	if (this.game.cache.checkImageKey('tiles') 
	&&  this.game.cache.checkTilemapKey('tilemap') 
	&&  this.map === undefined){
		this.map = game.add.tilemap('tilemap');
		this.map.addTilesetImage('demotiles', 'tiles'); //first arg is json name, second is load id
		//FIXME could try to load the same resources multiple times.
		for(var i = 0; i < this.map.objects['gameObjects'].length; i++){
			var gameObj =  this.map.objects['gameObjects'][i];
			var sprite  =  this.spriteFactory.createSprite(gameObj.name);
			sprite.preload();
			this.gameObjects.push(sprite);
		}
	}
};


Stage.prototype.create = function(){
    this.collidable = this.game.add.group();
    this.collidable.enableBody = true;

    this.map.createLayer('background');
	this.collidable = this.map.createLayer('collidable');
	this.collidable.resizeWorld();
	this.map.setCollisionBetween(0, this.map.width * this.map.height, true, 'collidable');
	
	//load sprites into map
	//FIXME write this better. possibly sprite.spawn()? is there a phaser way of doing this?
	for(var i = 0; i < this.map.objects['gameObjects'].length; i++){
		var gameObj =  this.map.objects['gameObjects'][i];
		var sprite  =  this.gameObjects[i];
		Phaser.Sprite.call(sprite, this.game, gameObj.x, gameObj.y, gameObj.name);
		sprite.create();
	}

	this.backgroundMusic = game.add.audio('Neoakushon');
	this.backgroundMusic.loopFull();
	this.game.camera.follow(this.gameObjects[0]);
};
Stage.prototype.update = function(){
	this.game.physics.arcade.collide(this.gameObjects[0], this.getCollidableLayer());
	for (var i = 0; i < this.gameObjects.length; i++){
		this.gameObjects[i].update();
	}
	this.game.debug.cameraInfo(this.game.camera, 32, 32);
    this.game.debug.spriteCoords(this.gameObjects[0], 32, 500);
};

/**
 * Returns the map layer intended to collide with other objects
 * @returns map layer that should be used for collision.
 */
Stage.prototype.getCollidableLayer = function(){
	return this.collidable;
};
