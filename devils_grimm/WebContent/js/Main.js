/**
 * This is the entry point of the game
 */

var game = new Phaser.Game(400, 300, Phaser.AUTO, '', { preload: preload, create: create, update: update });
var stage;
function preload() {
	stage = new Stage(game, new SpriteFactory(game));
	stage.preload();
}

function create() {
	game.physics.startSystem(Phaser.Physics.ARCADE);
	stage.create();
    game.scale.setMinMax(400, 300, 800, 600);
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
	
	//global game controls
	var fullscreenKey = game.input.keyboard.addKey(Phaser.Keyboard.F);
	fullscreenKey.onDown.add(goFullScreen, this);
	
	var volumeUpKey = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_ADD);
	volumeUpKey.onDown.add(increaseVolume, this);
	
	var volumeDownKey = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_SUBTRACT);
	volumeDownKey.onDown.add(decreaseVolume, this);
	
	game.sound.volume = 0.1; //TODO change to option
}

function update() {
	stage.update();
}

function goFullScreen() {

    if (game.scale.isFullScreen)
    {
    	game.scale.setMinMax(400, 300, 800, 600);
        game.scale.stopFullScreen();
    }
    else
    {
    	game.scale.setMinMax(400, 300, window.screen.width, window.screen.height);
        game.scale.startFullScreen(false);
    }

}

function increaseVolume(){
	if (game.sound.volume < 1){
		game.sound.volume += 0.1
	}
}

function decreaseVolume(){
	if (game.sound.volume > 0){
		game.sound.volume -= 0.1;
	}
}